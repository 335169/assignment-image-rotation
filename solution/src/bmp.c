#include "bmp.h"
#include "bmp_header.h"
#include "util.h"

#include <stdio.h>
#include <stdlib.h>


struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

enum read_status from_bmp(FILE *in, struct image *img) {
    static struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_ERROR;

    void *image_addr = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);
    if (!image_addr) {
        return READ_ERROR;
    }

    struct image* new_img = &((struct image) {.width = header.biWidth, .height = header.biHeight, .data = image_addr});
    uint64_t padding = calculate_padding(*new_img);

    for (size_t i = 0; i < new_img->height; i++) {
        if (fread(new_img->data + new_img->width * i, sizeof(struct pixel), new_img->width, in) != new_img->width) {
            free(image_addr);
            return READ_ERROR;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) return READ_ERROR;
    }

    *img = *new_img; // change img after success new_image
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    uint64_t padding = calculate_padding(*img);
    struct bmp_header header = create_bmp_header(img);


    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    struct pixel *img_addr = img->data;

    if (!img_addr) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + img->width * i, img->width * sizeof(struct pixel), 1, out) != 1) {
            free(img_addr);
            return WRITE_ERROR;
        }

        if (fwrite(img_addr, padding, 1, out) != 1) {
            free(img_addr);
            return WRITE_ERROR;
        }

    }

    return WRITE_OK;
}






