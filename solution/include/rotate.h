#ifndef ROTATE
#define ROTATE

#include "image.h"
#include <mm_malloc.h>

struct image rotate(struct image source, int degree);

#endif
