#include "util.h"

uint64_t calculate_padding(struct image const source) {
    return 4 - (source.width * 3) % 4 % 4;
}

void free_image(void * image) {
    free(image);
}
