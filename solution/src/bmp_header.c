#include "bmp_header.h"

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

struct bmp_header create_bmp_header(struct image const *img) {
    uint64_t padding = calculate_padding(*img);
    return (struct bmp_header) {  // bmp format
            .bfType = 0x4D42,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (img->width * sizeof(struct pixel)) * img->height + padding * img->height,
            .bfileSize = (img->width * sizeof(struct pixel)) * img->height + sizeof(struct bmp_header) +
                         padding * img->height
    };
}
