#include <mm_malloc.h>
#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(size_t width, size_t height) {
    void *new_image_addr = malloc(sizeof(struct pixel) * width * height);
    struct image new_image = (struct image) {.height = height, .width = width, .data = new_image_addr};
    return new_image;
}
