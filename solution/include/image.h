#ifndef IMAGE
#define IMAGE

#include <stdint.h>
#include <stdio.h>

struct pixel {
    uint8_t b, g, r;
} __attribute__((packed));

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(size_t width, size_t height);

#endif
