#ifndef UTIL
#define UTIL

#include "image.h"
#include "stdlib.h"
#include <stdint.h>

void free_image(void *image);

uint64_t calculate_padding(struct image const source);

#endif
