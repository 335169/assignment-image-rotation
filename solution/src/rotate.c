#include "image.h"
#include "stdio.h"

#include <mm_malloc.h>


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate_90(struct image const source) {
    void *new_image_addr = malloc(sizeof(struct pixel) * source.height * source.width);
    if (!new_image_addr) {
        fprintf(stderr, "ALLOCATION ERROR");
        return source;
    }

    struct image new_image = {.width = source.height, .height = source.width, .data = new_image_addr};

    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            new_image.data[source.height * j + (source.height - i - 1)] = source.data[j + i * source.width];
        }
    }

    return new_image;
}


struct image rotate(struct image const source, int degree) {
    struct image (*func)(struct image);
    func = NULL;
    if (degree == 90) func = rotate_90;


    if (!func) {
        fprintf(stderr, "ERROR no function with %d degree\nplease, add the new rotate function in rotate.c", degree);
        return source;
    }

    return func(source);
}

