#include "bmp.h"
#include "image.h"
#include "io.h"
#include "rotate.h"
#include "util.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("ERROR: argc != 3");
        return 1;
    }

    static FILE *input_file;
    static FILE *output_file;

    enum open_status status_input_file = open_file(&input_file, argv[1], "rb");  // read binary file
    enum open_status status_output_file = open_file(&output_file, argv[2], "wb");  // binary record file

    if (status_input_file != OPEN_OK || status_output_file != OPEN_OK) {
        fprintf(stderr, "ERROR open file");
        return 1;
    }

    static struct image input_image;
    enum read_status read_status_input_image = from_bmp(input_file, &input_image);
    if (read_status_input_image != READ_OK) {
        fprintf(stderr, "ERROR read status != READ_OK");
        return 1;
    }

    struct image output_image = rotate(input_image, 90);
    enum write_status write_status_output_file = to_bmp(output_file, &output_image);
    if (write_status_output_file != WRITE_OK) {
        fprintf(stderr, "ERROR write status != WRITE_OK");
        return 1;
    }


    enum close_status close_status_input_file = close_file(input_file);
    enum close_status close_status_output_file = close_file(output_file);

    if (close_status_input_file != CLOSE_OK || close_status_output_file != CLOSE_OK) {
        fprintf(stderr, "ERROR close file");
        return 1;
    }


    free_image(input_image.data);
    free_image(output_image.data);

    fprintf(stdout, "SUCCESS");
    return 0;
}
