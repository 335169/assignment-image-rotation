#include "io.h"


enum open_status open_file(FILE **file, const char *filename, const char *mode) {
    *file = fopen(filename, mode);
    if (!*file) return OPEN_ERROR;
    return OPEN_OK;
}

enum close_status close_file(FILE *file) {
    if (fclose(file) != 0) return CLOSE_ERROR;
    return CLOSE_OK;
}

